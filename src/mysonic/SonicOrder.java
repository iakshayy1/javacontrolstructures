/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysonic;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class SonicOrder {
    private String milkshakeName;
    private String orderType;
    private String contactNumber;
    private String size;
    private final double CLASSIC_SHAKE=3.0;
    private final double MASTER_SHAKE=3.5;
    private final double MASTER_BLAST=3.75;
    private final double WAFFLECONE_SUNDAE=4.0;
    private String shakeToppings;
    private String extraIceCream;
    
    /**Constructor with parameters
     *
     * @param milkshakeName Name of milkshake ordered
     * @param orderType Order type of customer
     * @param contactNumber Contact number of the customer
     * @param size Size of the milkshake
     */
    public SonicOrder(String milkshakeName, String orderType, String contactNumber, String size)
    {
        this.milkshakeName = milkshakeName;
        this.orderType = orderType;
        this.contactNumber = contactNumber;
        this.size = size;
    }

    /**Constructor with single parameter
     *
     * @param contactNumber Contact number of the customer
     */
    public SonicOrder(String contactNumber) 
    {
        this.contactNumber = contactNumber;
        this.milkshakeName = "Chocolate MilkShake";
        this.orderType = "Dine In";
        this.size = "Regular";
        
    }

    /**
     *no arg constructor
     */
    public SonicOrder() {
        
    }

    /**when we call getters in main method it gives return type
     *
     * @return the name of the milkshake
     */
    public String getMilkshakeName() {
        return milkshakeName;
    }

    /**when we call getters in main method it gives return type
     *
     * @return the type of order
     */
    public String getOrderType() {
        return orderType;
    }

    /**when we call getters in main method it gives return type
     *
     * @return the contact number of customer
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**when we call getters in main method it gives return type
     *
     * @return the size of milkshake
     */
    public String getSize() {
        return size;
    }

    /**when we call getters in main method it gives return type
     *
     * @return the toppings on the milkshake
     */
    public String getShakeToppings() {
        return shakeToppings;
    }

    /**when we call getters in main method it gives return type
     *
     * @return The flavor of ice cream scoops added to milk shake 
     */
    public String getExtraIceCream() {
        return extraIceCream;
    }

    /**When we call setters in the main method it sets the value.
     *
     * @param milkshakeName sets the name of the milkshake
     */
    public void setMilkshakeName(String milkshakeName) {
        this.milkshakeName = milkshakeName;
    }

    /**When we call setters in the main method it sets the value.
     *
     * @param orderType sets the type of order
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**When we call setters in the main method it sets the value.
     *
     * @param contactNumber sets contact number of customer
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    /**When we call setters in the main method it sets the value.
     *
     * @param size sets the size of milkshake.
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**When we call setters in the main method it sets the value.
     *
     * @param shakeToppings sets the toppings added on the milkshake.
     */
    public void setShakeToppings(String shakeToppings) {
        this.shakeToppings = shakeToppings;
    }

    /**When we call setters in the main method it sets the value.
     *
     * @param extraIceCream sets the icecream added to the scoops.
     */
    public void setExtraIceCream(String extraIceCream) {
        this.extraIceCream = extraIceCream;
    }
    
    /**on calling method in main method gives return type.
     *
     * @return the total number of toppings on the milkshake.
     */
    public double getTotalNumberToppings()
    {
        double count = 0;
       for(int i=0;i<shakeToppings.length();i++)
       {
           if(shakeToppings.charAt(i)==',')
           {
               
               count++;   
           }
           
       }
        return count + 1;
        
    }
    
    /**on calling method in main method gives return type
     *
     * @return the total number of scoops on the milkshake.
     */
    public double getTotalNumberScoops()
    {
      double count = 0;
       for(int i=0;i<extraIceCream.length();i++)
       {
           if(extraIceCream.charAt(i)==',')
           {
               
               count++;   
           }
           
       }
        return count + 1;
        
    }   
    
    /**on calling method in main method gives return type
     *
     * @return the total cost of scoops order by customer
     */
    public double getTotalScoopsCost()
    {
        double cost=0;
        String[] s1 = extraIceCream.split(",");
        for(int i=0;i<s1.length;i++)
        {
        switch(s1[i])
        {
            case "Vanilla" : 
            cost = cost + 1.0;
            break;
            case "Chocolate" :
            cost = cost + 1.25;
            break;
            case "Butterscotch":
            cost = cost + 2.5;
            break;
            case "Strawberry":
            cost = cost + 2.75;
            break;
            case "Blackberry":
            cost = cost + 2.25;
            break;
            case "Caramel":
            cost = cost + 2.5;
            break;
            case "Others":
            cost = cost + 3.0;
           
        }
        
    }
    return cost;
    }
    
    /**on calling method in main method gives return type
     *
     * @return the total cost of the toppings ordered by customer.
     */
    public double getTotalToppingsCost()
    {
        double cost = 0;
        String[] s2 = shakeToppings.split(",");
        for(int i=0;i<s2.length;i++)
        {
        
        if("M&M".equals(s2[i]))
        {
            cost=cost+0.75;
        }
          else  if("Oreo".equals(s2[i]))
        {
            cost=cost+0.65;
        }
              else  if("ChocolateChips".equals(s2[i]))
        {
            cost=cost+0.99;
        }
        else
        {
           cost = cost + 1.00; 
        {
            
        }
        }
        
    }
        return cost;
    }
        
    /**on calling method in main method gives return type
     *
     * @return the total cost of milkshake with toppings and scoops.
     */
    public double getTotalCost()
    {
        double totalCost = 0;
        if("MilkShake".equalsIgnoreCase(milkshakeName))
        {
            totalCost = CLASSIC_SHAKE;
        }
         else if("MasterShake".equalsIgnoreCase(milkshakeName))
            {
                totalCost = MASTER_SHAKE;
            }
             else if("MasterBlast".equalsIgnoreCase(milkshakeName))
                {
                    totalCost = MASTER_BLAST;
                }
               else    if("Wafflecone".equalsIgnoreCase(milkshakeName))
                    {
                        totalCost = WAFFLECONE_SUNDAE;
                    }
                    else
                    {
                        totalCost = CLASSIC_SHAKE;
                    }
                
        String[] str = shakeToppings.split(",");
        String[] str1 = extraIceCream.split(",");
        if(str.length > 2)
        {
            totalCost = totalCost + 1;
        }
        if(str1.length > 1)
        {
            totalCost = (totalCost * 1.1);
        }
        totalCost = totalCost + getTotalScoopsCost();
        totalCost = totalCost + getTotalToppingsCost();
   
        if(getOrderType().equalsIgnoreCase("Dine In"))
    {
        totalCost = totalCost * 0.9;
    }
    else
    {
        totalCost = totalCost * 1.1;
    }
    return totalCost;
    }
    
    
    
        
     /**Returns the string representation of a variable or operation
     * 
     * @return Returns the operation when we call toString method in main method
     */
    @Override
    public String toString() {
        return "\nYour Order :" + "\nContact Number of Customer: "+contactNumber+"\nMilkshake Ordered by Customer : "+milkshakeName+"\nSize of Milkshake : " +size+"\nToppings for milk shake : " +shakeToppings+ "\nExtra Scoops of Ice-Cream : " +extraIceCream+"\n********************************************"+"\nTotal Cost of YourOrder : "+getTotalCost();
    }


    }

    


    

    
    
    
    
    

    
    
    
    
    

